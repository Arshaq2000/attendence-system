// import React, { Component } from 'react';

import React from 'react'

//import image from './images/diaries.png';
const axios = require("axios");
class ReactUploadImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            imgUrl: null,
        };
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    onFormSubmit(e) {
        let { imgUrl } = this.state;
        e.preventDefault();
        const formData = new FormData();
        formData.append('myImage', this.state.file);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        //post file
        axios.post("http://localhost:4001/upload", formData, config)
            .then((response) => {
                alert("The file is successfully uploaded");
                // console.log(response)

                //get file
                console.log("get File is Working")

                fetch("http://localhost:4001/get/file")
                    .then((success) => success.json())
                    .then((res) => {

                        //console.log(res[0])
                        let imgUrl = res[0].image;

                        this.setState({ imgUrl }, () => console.log(imgUrl));

                    }).catch((err) => console.log(err))

                //get file




            }).catch((error) => {
                console.log(error)
            });//post file


    }
    onChange(e) {
        this.setState({ file: e.target.files[0] });
    }

    render() {
        console.log(this.state.imgUrl)
        return (
            <form onSubmit={this.onFormSubmit}>
                <h1>File Upload</h1>
                <p><img alt="" src={this.state.imgUrl} /></p>
                <input type="file" name="myImage" onChange={this.onChange} />
                <button type="submit">Upload</button>
            </form>
        )
    }
}

export default ReactUploadImage;