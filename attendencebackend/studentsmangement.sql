-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2019 at 03:03 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentsmangement`
--

-- --------------------------------------------------------

--
-- Table structure for table `daires`
--

CREATE TABLE `daires` (
  `id` int(11) NOT NULL,
  `class` varchar(10) NOT NULL,
  `subjects` varchar(20) NOT NULL,
  `text` varchar(50) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daires`
--

INSERT INTO `daires` (`id`, `class`, `subjects`, `text`, `image`, `document`) VALUES
(1, 'one', 'Science', 'Lesson Text Here', 'myImage-1551876920943.jpg', 'Images/myImage-1551861941586.docx'),
(2, 'one', 'English', 'Lesson Text Here', 'Images/myImage-1551858009486.png', NULL),
(3, 'one', 'Mathematics', 'Lesson Text Here', NULL, 'Images/myImage-1551862010240.docx'),
(4, 'one', 'Physics', 'Lesson Text Here', NULL, NULL),
(5, 'one', 'Social Studies', 'Lesson Text Here', NULL, NULL),
(6, 'Two', 'English', 'Lesson Text Here', 'Images/myImage-1551859728331.jpg', NULL),
(7, 'Two', 'Physics', 'Lesson Text Here', 'Images/myImage-1551861621743.png', NULL),
(8, 'Two', 'Mathematics', 'Lesson Text Here', NULL, NULL),
(9, 'Two', 'Social Studies', 'Lesson Text Here', NULL, NULL),
(10, 'Two', 'Science', 'Lesson Text Here', NULL, NULL),
(11, 'Three', 'Science', 'Lesson Text Here', NULL, NULL),
(12, 'Three', 'English', 'Lesson Text Here', NULL, NULL),
(13, 'Three', 'Geography', 'Lesson Text Here', NULL, NULL),
(14, 'Three', 'Mathematics', 'Lesson Text Here', NULL, NULL),
(15, 'Three', 'Social Studies', 'Lesson Text Here', NULL, NULL),
(16, 'Four', 'Science', 'Lesson Text Here', NULL, NULL),
(17, 'Four', 'English', 'Lesson Text here', NULL, NULL),
(18, 'Four', 'Mathematics', 'Lesson Text Here', NULL, NULL),
(19, 'Four', 'Geography', 'Lesson Text Here', NULL, NULL),
(20, 'Four', 'Social Studies', 'Lesson Text Here', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daires`
--
ALTER TABLE `daires`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daires`
--
ALTER TABLE `daires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
